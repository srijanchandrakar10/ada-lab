#include <stdio.h>
#include <string.h>

void newMatrix(int k, int V, int adj[V][V], int new_adj[V][V]){
	for(int i = 0; i < V; i++)
		for(int j = 0; j < V; j++)
			if(i != k && j != k)
				new_adj[i][j] = adj[i][j];
}

void dfs(int s, int V, int visited[V], int adj[V][V]){
    visited[s] = 1;
    for (int i = 0; i <= V; i++)
        if (adj[s][i] == 1 && visited[i] != 1)
            dfs(i, V, visited, adj);
}

void find_articulation_points(int V, int adj[V][V], int isArticulationPoint[V]){
	for(int i = 0; i < V; i++){
		int new_adj[V][V], visited[V];
		memset(new_adj, 0, V * V *sizeof(int));
		memset(visited, 0, V * sizeof(int));
		
		newMatrix(i ,V, adj, new_adj);
		dfs((i == 0) ? 1 : 0, V, visited,  new_adj);
		for(int j = 0; j < V; j++)
			if(visited[j] == 0 && j != i){
				isArticulationPoint[i] = 1;
				break;
			}
	}
}

int main(){
    int i, j;
    int V;
    printf("\nEnter the value of number of vertices:");
    scanf("%d", &V);
    int adj[V][V], isArticulationPoint[V];
    printf("\nEnter the adjacency matrix\n");
    for (int i = 0; i < V; i++){
    	isArticulationPoint[i] = 0;
        for (int j = 0; j < V; j++)
            scanf("%d", &adj[i][j]);
    }
    int counter = 0;
    find_articulation_points(V, adj, isArticulationPoint);
    for(int i = 0; i < V; i++){
    	if(isArticulationPoint[i] == 1){
    		if(counter == 0)
    			printf("Articulation Points are:\n"), counter++;
    		printf("%d ", i + 1);
    	}
    }
    if(counter == 0)
    	printf("It's a biconnected graph\n");
    return 0;
}
